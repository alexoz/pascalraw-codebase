import os
from BeautifulSoup import BeautifulSoup as bsoup
from pymatbridge import Matlab
from datetime import datetime
import shutil
import subprocess
import re
import fileinput

#Global Constants
HOME_PATH = '/Users/alexoz/Dropbox/research/murmann/cv/pascalraw'
MATLAB_PATH = '/Users/alexoz/Dropbox/research/murmann/cv/pascalraw/matlab'
MATLAB_EXECUTABLE_PATH = '/Applications/MATLAB_R2014b.app/bin/matlab'
PYTHON_PATH = '/Users/alexoz/Dropbox/research/murmann/cv/pascalraw/python'
RAW_FORMAT = '.nef'
PGM_FORMAT = '.pgm'
ANNOTATION_FORMAT = '.xml'
RAW_IMAGE_WIDTH = 6000
RAW_IMAGE_HEIGHT = 4000
PASCAL_IMAGE_NUMBER_STRING_LENGTH = 6
PASCAL_IMAGE_STRING_LENGTH = 11
VALID_OBJECT_CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse','motorbike','person','pottedplant', 'sheep','sofa','train','tvmonitor']

class BoundingBox(object):
    """This class is the simplest representation of a bounding box, which includes the object class, bounding box coordinates,
    object pose, and truncated, occluded, and difficult tags."""
    def __init__(self, initialX1=0, initialY1=0, initialX2=0, initialY2=0, initialTruncated=0, initialOccluded=0, initialDifficult=0, initialObjectClass='', initialObjectPose=''):
        self.x1 = initialX1
        self.y1 = initialY1
        self.x2 = initialX2
        self.y2 = initialY2
        self.objectClass = initialObjectClass
        self.objectPose = initialObjectPose
        self.truncated = initialTruncated
        self.occluded = initialOccluded
        self.difficult = initialDifficult

def createDatabase(databasePath, downsamplingFactor):
    """Creates a new database assuming the folder structure described in README.txt
    
    Creates a new database by creating relevant directories, converting original image file extensions to 
    lowercase, renaming original images according to PASCAL VOC format, converting raw images to .pgm format,
    downsampling .pgm images to .png, and downsampling .jpg images. 

    Args:
        databasePath: a string that specifies the database root directory path. 
        downsamplingFactor: an integer value that specifies the degree of downsampling. For example 2 would invoke imresize(im, 0.5).
    Returns:

    Raises:

    """ 
    #define paths
    rawPath = os.path.join(databasePath, 'original', 'raw')
    jpgOriginalPath = os.path.join(databasePath, 'original', 'jpg')
    pgmPath = os.path.join(databasePath, 'original', 'pgm')
    pngPath = os.path.join(databasePath, 'png')
    jpgPath = os.path.join(databasePath, 'jpg')
    annotationsPath = os.path.join(databasePath, 'annotations')
    mainPath = os.path.join(databasePath, 'Main')

    print '\n***\nMaking directories:\n***\n'
    #make directories'
    if not os.path.exists(pgmPath):
        os.makedirs(pgmPath)
        print pgmPath
    if not os.path.exists(pngPath):
        os.makedirs(pngPath)
        print pngPath
    if not os.path.exists(jpgPath):
        os.makedirs(jpgPath)
        print jpgPath
    if not os.path.exists(annotationsPath):
        os.makedirs(annotationsPath)
        print annotationsPath
    if not os.path.exists(mainPath):
        os.makedirs(mainPath)
        print mainPath

    # Convert image file extensions to lowercase.
    convertFileExtensionsToLowerCase(rawPath)
    convertFileExtensionsToLowerCase(jpgOriginalPath)

    # Rename images according to PASCAL VOC format
    renameImages(rawPath, jpgOriginalPath)

    # Covert raw images to pgm
    convertRawToPgm(rawPath, pgmPath)

    # Downsample images
    downsampleJpg(jpgOriginalPath, jpgPath, downsamplingFactor)
    downsamplePgmToPng(pgmPath, pngPath, downsamplingFactor)

def formatInt(number):
    """This function takes an Int as an argument and returns a formatted string based on the PASCAL naming convention.
    For example, 1 would become 000001, and 103 would become 000103."""

    # bounds check:
    if ((type(number) is not int) or (number >= pow(10, PASCAL_IMAGE_NUMBER_STRING_LENGTH)) or (number <= 0)):
        print "Error. formatInt argument must be an Int in the range of 1 to " + str(pow(10, PASCAL_IMAGE_NUMBER_STRING_LENGTH) - 1)
        return

    result = str(number); 
    inputLength = len(result);
    for x in range(0, PASCAL_IMAGE_NUMBER_STRING_LENGTH - inputLength):
        result = '0' + result

    return result

def getInt(pascalString):
    """This function takes a formatted string based on the PASCAL naming convention as an argument and returns an Int.
    For example, 2014_000001 would return 1, and 2014_000103 would return 103."""

    #check if the file matches the PASCAL naming convention (e.g. 2014_000001)
    match = re.search(r'\d{4}_\d{6}', pascalString)
    #bounds check
    if ((type(pascalString) is not str) or (len(pascalString) != PASCAL_IMAGE_STRING_LENGTH) or (match is None)):
        print "Error. getInt argument must be a valid PASCAL formatted string (e.g. 2014_000001)"
        return
    else:         
        return int(pascalString[5:])

def getYear(pascalString):
    """This function takes a formatted string based on the PASCAL naming convention as an argument and returns an Int
    corresponding to the year. For example, 2014_000001 would return 2014."""

    #check if the file matches the PASCAL naming convention (e.g. 2014_000001)
    match = re.search(r'\d{4}_\d{6}', pascalString)
    #bounds check
    if ((type(pascalString) is not str) or (len(pascalString) != PASCAL_IMAGE_STRING_LENGTH) or (match is None)):
        print "Error. getInt argument must be a valid PASCAL formatted string (e.g. 2014_000001)"
        return
    else:         
        return int(pascalString[:4])

def isPascalFormat(fileName):
    """This function takes a filename as an argument and returns a boolean value based on whether the filename follows
    the PASCAL naming convention. For example, 2014_000001 would return True, and DSC_0001 would return False."""
    #bounds check:
    if ((type(fileName) is not str) or (len(fileName) != PASCAL_IMAGE_STRING_LENGTH)):
        return False
    else:
        # check if the file matches the PASCAL naming convention (e.g. 2014_000001)
        match = re.search(r'\d{4}_\d{6}',fileName)
        return match is not None
    
def containsClass(annotationFilePath, objectClass):
    """Checks if xml annotation file contains an object of the given class.
    
    Parses xml annotation file using BeautifulSoup. Checks whether the xml annotation file contains an 
    object of the specified class or not. Returns True if it does and False if it does not.

    Args:
        annotationFilePath: a string that specifies the xml annotation file path.
        objectClass: a string that specifies the object class. 
    Returns:
        Boolean value representing whether or not the xml annotation file contains an object of the 
        specified class.
    Raises:

    """ 
    xmlText = open(annotationFilePath).read()
    soup = bsoup(xmlText)
    nameTags = soup.findAll('name')
    objectClassesInXml = [nameTag.text for nameTag in nameTags]
    if objectClass in objectClassesInXml:
        return True
    else:
        return False

def getObjectCount(annotationFilePath, objectClass):
    """Counts number of objects of the given class in an xml annotation file.
    
    Parses xml annotation file using BeautifulSoup. Counts and returns the number of objects of the 
    given class contained in the xml annotation file.

    Args:
        annotationFilePath: a string that specifies the xml annotation file path.
        objectClass: a string that specifies the object class. 
    Returns:
        Int value representing the number of objects of the given class in the xml annotation file.
    Raises:

    """ 
    annotationFile = open(annotationFilePath)
    xmlText = annotationFile.read()
    soup = bsoup(xmlText)
    nameTags = soup.findAll('name')
    objectClassesInXml = [nameTag.text for nameTag in nameTags]
    count = 0
    for currentObjectClass in objectClassesInXml:
        if currentObjectClass == objectClass:
            count += 1
    annotationFile.close()
    return count

def getTotalObjectCount(annotationsPath, objectClass):
    """Counts number of objects of the given class in all .xml annotation files in annotationsPath.

    Parses all .xml annotation files in annotationsPath using BeautifulSoup. Counts and returns the total 
    number of objects of the given class contained in all xml annotation files.

    Args:
        annotationsPath: a string that specifies the directory containing the original .xml annotation files.
        objectClass: a string that specifies the object class. 
    Returns:
        Int value representing the total number of objects of the given class in all .xml annotation files in 
        annotationsPath.
    Raises:

    """
    count = 0
    for currentFile in os.listdir(annotationsPath):  
        fileName, fileExtension = os.path.splitext(currentFile)
        if (fileExtension == ANNOTATION_FORMAT): 
            count += getObjectCount(currentFile, objectClass)
    return count

def getBoundingBoxes(annotationFilePath):
    """Finds and returns all objects and corresponding bounding box coordinates in an xml annotation file.
    
    Parses xml annotation file using BeautifulSoup. Finds and all objects and corresponding bounding 
    box coordinates and returns a list of BoundingBox objects. 

    Args:
        annotationFilePath: a string that specifies the xml annotation file path.
    Returns:
        list of BoundingBox objects.
    Raises:

    """ 
    xmlText = open(annotationFilePath).read()
    soup = bsoup(xmlText)
    nameTags = soup.findAll('name')
    xmaxTags = soup.findAll('xmax')
    xminTags = soup.findAll('xmin')
    ymaxTags = soup.findAll('ymax')
    yminTags = soup.findAll('ymin')

    boundingBoxList = []

    for i, val in enumerate(nameTags):
        box = BoundingBox()
        box.objectClass = nameTags[i].text
        box.x1 = int(xminTags[i].text)
        box.x2 = int(xmaxTags[i].text)
        box.y1 = int(yminTags[i].text)
        box.y2 = int(ymaxTags[i].text)
        boundingBoxList.append(box)

    return boundingBoxList

def writeTrainVal(inputPath, outputPath, groupingFactor=1):
    """Writes trainval, train, and val files for all xml annotation files in directory according to PASCAL VOC format.

    Parses all xml annotation files using BeautifulSoup. Writes the general trainval, train, and val files. Then loops 
    through all valid object classes, and for each object class, writes the object class trainval, train, and val files 
    by checking whether each xml annotation file contains an object of that class or not. Images are split between train
    and val according to whether or not their file numbers are even or odd. This ensures that objects of a given class
    will be evenly split even if a particular object class appears in many consecutive images. All trainval, train, and 
    val files are written to the outputPath. 

    Args:
        inputPath: a string that specifies the directory containing the xml annotation files.
        outputPath: a string that specifies the directory in which to write the output trainval, train, and val files. 
    Returns:

    Raises:

    """
    #bounds check
    if ((type(groupingFactor) is not int) or (groupingFactor < 1)):
        print "Error. groupingFactor parameter must be an Int greater than or equal to 1."
        return

    #this current implementation is obviously inefficient. could go back and optimize this later if needed . 
    xmlFiles = []
    for currentFile in os.listdir(inputPath):
        fileName, fileExtension = os.path.splitext(currentFile)
        if (fileExtension.lower() == ".xml"):
            xmlFiles.append(currentFile)

    #write general trainval file
    outputFile = os.path.join(outputPath, 'trainval.txt')
    f = open(outputFile, 'w')
    for currentFile in xmlFiles:
        fileName, fileExtension = os.path.splitext(currentFile)
        f.write(fileName + '\n')
    f.close()

    #write general train file
    outputFile = os.path.join(outputPath, 'train.txt')
    f = open(outputFile, 'w')
    for currentFile in xmlFiles:
        fileName, fileExtension = os.path.splitext(currentFile)
        if ((getInt(fileName)/groupingFactor) % 2 == 0):
            f.write(fileName + '\n')
    f.close()

    #write general val file
    outputFile = os.path.join(outputPath, 'val.txt')
    f = open(outputFile, 'w')
    for currentFile in xmlFiles:
        fileName, fileExtension = os.path.splitext(currentFile)
        if ((getInt(fileName)/groupingFactor) % 2 == 1):
            f.write(fileName + '\n')
    f.close()

    #write class specific trainval files
    for objectClass in VALID_OBJECT_CLASSES:
        outputFile = os.path.join(outputPath, objectClass + '_trainval.txt')
        f = open(outputFile, 'w')
        for currentFile in xmlFiles:
            fileName, fileExtension = os.path.splitext(currentFile)
            if containsClass(os.path.join(inputPath, currentFile), objectClass):
                f.write(fileName + '\t1\n')
            else:
                f.write(fileName + '\t-1\n')

    #write class specific train files (all even numbered annotation files)
    for objectClass in VALID_OBJECT_CLASSES:
        outputFile = os.path.join(outputPath, objectClass + '_train.txt')
        f = open(outputFile, 'w')
        for currentFile in xmlFiles:
            fileName, fileExtension = os.path.splitext(currentFile)
            if ((getInt(fileName)/groupingFactor) % 2 == 0):
                if containsClass(os.path.join(inputPath, currentFile), objectClass):
                    f.write(fileName + '\t1\n')
                else:
                    f.write(fileName + '\t-1\n')

    #write class specific val files (all odd numbered annotation files)
    for objectClass in VALID_OBJECT_CLASSES:
        outputFile = os.path.join(outputPath, objectClass + '_val.txt')
        f = open(outputFile, 'w')
        for currentFile in xmlFiles:
            fileName, fileExtension = os.path.splitext(currentFile)
            if ((getInt(fileName)/groupingFactor) % 2 == 1):
                if containsClass(os.path.join(inputPath, currentFile), objectClass):
                    f.write(fileName + '\t1\n')
                else:
                    f.write(fileName + '\t-1\n')

def filterTrainVal(filterPath, trainValPath, outputPath):
    """Filters trainval, train, and val files such that they only contain images that appear in the specified filter file. 

    Parses images from a filter file given by filterPath and images from either a train, val, or trainval file given by
    the trainValPath. Then filters the train, val, or trainval file such that it only contains images that appear in the
    specified filter file and writes this filtered version to outputPath. The original train, val, or trainval file is not
    rewritten. 

    Args:
        filterPath: a string that specifies the filter file path.
        trainValPath: a string that specifies the trainval, train, or val file path. 
        outputPath: a string that specifies the directory in which to write the output filtered trainval, train, or val files. 

    Returns:

    Raises:

    """
    f = open(filterPath, 'r')
    #call strip to remove newline characters and replace to remove .xml file extensions
    validImages = [line.strip().replace('.xml', '') for line in f]
    f.close()
    f = open(trainValPath, 'r')
    #call strip to remove newline characters
    trainValImages = [line.strip() for line in f]
    f.close()
    f = open(os.path.join(outputPath, os.path.basename(trainValPath)), 'w')
    for image in trainValImages:
        if image in validImages:
            f.write(image + '\n')

def copyFilteredAnnotations(filterPath, annotationFilePath, outputPath):
    """Copies .xml annotation files that appear in the specified filter file to outputPath. 

    Args:
        filterPath: a string that specifies the filter file path.
        annotationFilePath: a string that specifies the .xml annotation file path. 
        outputPath: a string that specifies the directory in which to copy the filtered .xml annotation files. 
    Returns:

    Raises:

    """
    f = open(filterPath, 'r')
    #call strip to remove newline characters
    validAnnotations = [line.strip() for line in f]
    f.close()
    for annotation in validAnnotations:
        shutil.copyfile(os.path.join(annotationFilePath, annotation), os.path.join(outputPath, annotation))

def convertFileExtensionsToLowerCase(filePath):
    """Renames all files in directory such that extensions are lowercase.

    Renames all files in directory such that extensions are lowercase. For example, file.JPG will be renamed as 
    file.jpg.

    Args:
        filePath: a string that specifies the directory containing the files.
    Returns:

    Raises:

    """   
    print '\n***\nConverting file extensions in %s to lowercase:\n***\n' % (filePath,)
    for currentFile in os.listdir(filePath):
        fileName, fileExtension = os.path.splitext(currentFile)
        os.rename(os.path.join(filePath, currentFile), os.path.join(filePath, fileName + fileExtension.lower()))

def renameImages(rawPath, jpgPath, startingCount=1):
    """Renames all RAW and jpg files in directory according to PASCAL VOC format.

    Renames all RAW files in directory rawPath and JPG files in the directory jpgPath according to PASCAL 
    VOC format. Reads RAW files into a list, and then renames each in order starting with startingCount. 
    For example, if DSC_001.nef is the first image read into the list, it will be renamed as 2014_000001.nef. 
    If corresponding JPG files also exist in the jpgPath, they will also be renamed. Note that files in the 
    rawPath and jpgPath that are already named according to the PASCAL VOC format at the time this function
    is called will not be renamed. Instead, this function will find the highest count associated with 
    existing PASCAL VOC formatted files from the current year and rename all unformatted files starting with
    count + 1. For example, if the current year is 2014, and 2014_000003.nef exists in rawPath, unformatted 
    files will be renamed as 2014_000004.nef, 2014_000005.nef, etc. If startingCount is less than or equal
    to the highest count, it will be ignored.

    Args:
        rawPath: a string that specifies the directory containing the RAW files.
        jpgPath: a string that specifies the directory containing the JPG files.
        startingCount: an integer that specifies the minimum image index to rename. 
    Returns:

    Raises:

    """
    print '\n***\nRenaming images according to PASCAL VOC format:\n***\n'
    currentYear = datetime.now().year
    count = startingCount;
    files = os.listdir(rawPath)
    #iterate through all files to find the max image number for the current year. set count appropriately.
    for currentFile in files:
        fileName, fileExtension = os.path.splitext(currentFile)
        if ((fileExtension == RAW_FORMAT) and (isPascalFormat(fileName)) and (getYear(fileName) == currentYear)):
            if (getInt(fileName) >= count):
                count = getInt(fileName) + 1

    for currentFile in os.listdir(rawPath):
        fileName, fileExtension = os.path.splitext(currentFile)
        if (fileExtension == RAW_FORMAT and (not isPascalFormat(fileName))):
            os.rename(os.path.join(rawPath, currentFile), os.path.join(rawPath, str(currentYear) + '_' + formatInt(count) + fileExtension))
            #check if corresponding JPG file exists. If so, rename it. 
            if os.path.isfile(os.path.join(jpgPath, fileName + '.jpg')):
                print fileName + ' => ' + str(currentYear) + '_' + formatInt(count)
                os.rename(os.path.join(jpgPath, fileName + '.jpg'), os.path.join(jpgPath, str(currentYear) + '_' + formatInt(count) + '.jpg'))
            count += 1

def convertRawToPgm(rawPath, pgmPath):
    """Converts all RAW files in rawPath to PGM (.pgm) format using dcraw.

    Converts all RAW files in directory rawPath to PGM (.pgm) format using dcraw,
    places PGM files in pgmPath. Keeps the same file names. For example, 000001.nef will
    be converted to 000001.pgm.

    Args:
        rawPath: a string that specifies the directory containing the RAW files.
        pgmPath: a string that specifies the directory in which to place the PGM files.

    Returns:

    Raises:

    """
    print '\n***\nConverting RAW files to PGM format:\n***\n'
    for currentFile in os.listdir(rawPath):
        fileName, fileExtension = os.path.splitext(currentFile)
        if (fileExtension.lower() == RAW_FORMAT):
            #only convert the file if it has not already been converted.
            if os.path.isfile(os.path.join(pgmPath, fileName + '.pgm')):
                print fileName + ' (converted previously)'
            else:
                print fileName
                os.system("dcraw -E -4 " + os.path.join(rawPath, currentFile))
                os.system("mv " + os.path.join(rawPath, fileName + '.pgm') + " " + pgmPath)

def downsamplePgmToPng(pgmPath, pngPath, downsamplingFactor):
    """Downsamples all PGM (.pgm) files in pgmPath and saves downsampled images as PNG (.png) files in pngPath

    Downsamples all .pgm files in directory pgmPath by downsamplingFactor using the
    Matlab function downsample.m, called through pymatbrdige. Saves downsampled images
    as PNG (.png) files in pngPath. Keeps the same file names. For example, 2014_000001.pgm will
    be converted to 2014_000001.png.

    Args:
        pgmPath: a string that specifies the directory containing the PGM files.
        pngPath: a string that specifies the directory in which to place the PNG files. 
        downsamplingFactor: an integer value that specifies the degree of downsampling.
    Returns:

    Raises:

    """

    #check to see if downsampling factor is valid
    if ((RAW_IMAGE_WIDTH%downsamplingFactor is 0) and (RAW_IMAGE_HEIGHT%downsamplingFactor is 0)):
        print '\n***\nDownsampling PGM files by factor of %f and saving in PNG format:\n***\n' % (downsamplingFactor,)
        mlab = Matlab(executable=MATLAB_EXECUTABLE_PATH)
        mlab.start()
        #clean up inputPath. add trailing backslash in case it doesn't already exist. this is required in order for
        #MATLAB's dir() function to work properly.
        pgmPath = os.path.normpath(pgmPath) + os.sep
        pngPath = os.path.normpath(pngPath) + os.sep
        argumentMap = {'pgmPath': pgmPath, 'pngPath': pngPath, 'downsamplingFactor': downsamplingFactor}
        mlab.run_func(MATLAB_PATH + '/downsamplePgmToPng.m', argumentMap)
        mlab.stop()
    else:
        print 'Error: downsampling factor must be an exact factor of raw image height (' + str(RAW_IMAGE_HEIGHT) + ') and width (' + str(RAW_IMAGE_WIDTH) + ').'

def downsampleJpg(jpgPath, jpgDownsampledPath, downsamplingFactor):
    """Downsamples all JPEG (.jpg) files in jpgPath and saves downsampled images as jpg (.jpg) files in jpgDownsampledPath.

    Downsamples all .jpg files in directory jpgPath by downsamplingFactor using the
    Matlab function downsampleJpg.m, called through pymatbrdige. Saves downsampled images
    as JPEG (.jpg) files in jpgDownsampledPath. Keeps the same file names. For example, 2014_000001.jpg will
    be converted to 2014_000001.jpg.

    Args:
        jpgPath: a string that specifies the directory containing the original JPG files.
        jpgDownsampledPath: a string that specifies the directory in which to place the downsampled JPG files. 
        downsamplingFactor: an integer value that specifies the degree of downsampling. For example 2 would invoke imresize(im, 0.5).
    Returns:

    Raises:

    """

    if ((RAW_IMAGE_WIDTH%downsamplingFactor is 0) and (RAW_IMAGE_HEIGHT%downsamplingFactor is 0)):
        print '\n***\nDownsampling JPG files by factor of %f:\n***\n' % (downsamplingFactor,)
        mlab = Matlab(executable=MATLAB_EXECUTABLE_PATH)
        mlab.start()
        #clean up inputPath. add trailing backslash in case it doesn't already exist. this is required in order for
        #MATLAB's dir() function to work properly.
        jpgPath = os.path.normpath(jpgPath) + os.sep
        jpgDownsampledPath = os.path.normpath(jpgDownsampledPath) + os.sep
        argumentMap = {'jpgPath': jpgPath, 'jpgDownsampledPath': jpgDownsampledPath, 'downsamplingFactor': downsamplingFactor}
        mlab.run_func(MATLAB_PATH + '/downsampleJpg.m', argumentMap)
        mlab.stop()
    else:
        print 'Error: downsampling factor must be an exact factor of raw image height (' + str(RAW_IMAGE_HEIGHT) + ') and width (' + str(RAW_IMAGE_WIDTH) + ').'




