function dummyVar = downsampleJpg(args)
    // Global Constants
    HORIZONTAL_CROP_LEFT = 8;
    HORIZONTAL_CROP_RIGHT = 8;
    VERTICAL_CROP_TOP = 0;
    VERTICAL_CROP_BOTTOM = 0;
    files = dir(strcat(args.jpgPath,'*.jpg'));
    for i = 1:numel(files)
        [pathstr,name,ext] = fileparts(files(i).name);
        filename = files(i).name;
        %only downsample the file if it has not already been downsampled.
        if exist(strcat(args.jpgDownsampledPath, filename))
            fprintf(strcat(filename, ' (downsampled previously)\n'));
        else
            fprintf(strcat(filename, '\n'));
            im = imread(strcat(args.jpgPath, filename));
            im = im(1+VERTICAL_CROP_TOP:end-VERTICAL_CROP_BOTTOM, 1+HORIZONTAL_CROP_LEFT:end-HORIZONTAL_CROP_RIGHT, :);
            %note that the inverse of the downsampling factor is used to achieve compatibility with imresize
            im = imresize(im, 1.0/args.downsamplingFactor);

            % Rotate image if necessary. Orientation values based on http://www.impulseadventure.com/photo/exif-orientation.html
            imInfo = imfinfo(strcat(args.jpgPath, filename));
            switch imInfo.Orientation
                case 1

                case 2
                    im = im(:,end:-1:1,:);
                case 3
                    im = imrotate(im,180);
                    im = im(:,end:-1:1,:);
                case 4
                    im = im(:,end:-1:1,:);

                case 6
                    im = imrotate(im,-90);            
                case 5
                    im = im(:,end:-1:1,:);
                    im = imrotate(im,-90);            

                case 8
                    im = imrotate(im,90);            
                case 7
                    im = imrotate(im,90);            
                    im = im(:,end:-1:1,:);
            end
            imwrite(im, strcat(args.jpgDownsampledPath, filename), 'Quality', 100);
        end
    end
    dummyVar = 0;
end
